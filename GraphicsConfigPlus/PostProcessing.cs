using HarmonyLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;
using UnityStandardAssets.ImageEffects;

namespace GraphicsConfigPlus
{
    public static class PostProcessing
    {
        private static PostProcessingBehaviour postProcessingBehaviour;
        private static CameraEffects cameraEffects;

        public static void OnReload()
        {
            SetBloom();
            SetSunShafts(null);
            SetSSAO();
            SetMotionBlur();
            SetAntiAliasing();
            SetCA();
            PostApplySettings();
        }

        [HarmonyPatch(typeof(CameraEffects), "Awake")]
        [HarmonyPrefix]
        private static void Awake(CameraEffects __instance)
        {
            postProcessingBehaviour = __instance.GetComponent<PostProcessingBehaviour>();
        }

        [HarmonyPatch(typeof(CameraEffects), "ApplySettings")]
        [HarmonyPostfix]
        private static void PostApplySettings()
        {
            if (postProcessingBehaviour.profile.colorGrading == null)
                return;

            ColorGradingModel.Settings settings = postProcessingBehaviour.profile.colorGrading.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----Misc----");
                Debug.Log($"GammaCorrection: {(settings.colorWheels.linear.gamma).ToString("0.")} to {Main.gammaCorrection.Value}");
            }

            settings.colorWheels.linear.gamma = Main.gammaCorrection.Value;

            postProcessingBehaviour.profile.colorGrading.settings = settings;

            ApplyFogSettings();

            postProcessingBehaviour.enabled = !Main.disablePostProcessing.Value;
        }

        private static void ApplyFogSettings()
        {
            if (SceneManager.GetActiveScene().name == "start")
                return;

            if (Main.printValues.Value)
            {
                Debug.Log($"FogColor: {RenderSettings.fogColor} to {Main.fogColor.Value}");
                Debug.Log($"FogDensity: {RenderSettings.fogDensity} to {Main.fogDensity.Value}");
                Debug.Log($"FogStartDistance: {RenderSettings.fogStartDistance} to {Main.fogStartDistance.Value}");
                Debug.Log($"FogEndDistance: {RenderSettings.fogEndDistance} to {Main.fogEndDistance.Value}");
                Debug.Log($"FogMode: {RenderSettings.fogMode} to {(FogMode)Main.fogMode.Value}");
                Debug.Log($"Fog: {postProcessingBehaviour.profile.fog.enabled} to {Main.fog.Value}");
            }

            RenderSettings.fogColor = Main.fogColor.Value;
            RenderSettings.fogDensity = Main.fogDensity.Value;
            RenderSettings.fogStartDistance = Main.fogStartDistance.Value;
            RenderSettings.fogEndDistance = Main.fogEndDistance.Value;
            RenderSettings.fogMode = (FogMode)Main.fogMode.Value;
            postProcessingBehaviour.profile.fog.enabled = Main.fog.Value;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetSunShafts")]
        [HarmonyPrefix]
        private static void SetSunShafts(CameraEffects __instance)
        {
            if (__instance != null && cameraEffects == null)
                cameraEffects = __instance;

            SunShafts component = cameraEffects.GetComponent<SunShafts>();

            if (component == null)
                return;

            if (Main.printValues.Value)
            {
                Debug.Log("----SunShafts settings----");
                Debug.Log($"SunShaftsResolution: {component.resolution} to {(SunShafts.SunShaftsResolution)Main.sunShaftsResolution.Value}");
                Debug.Log($"SunShaftsScreenBlendMode: {component.screenBlendMode} to {(SunShafts.ShaftsScreenBlendMode)Main.sunShaftsScreenBlendMode.Value}");
                Debug.Log($"SunShaftsMaxRadius: {component.maxRadius} to {Main.sunShaftsMaxRadius.Value}");
                Debug.Log($"SunShaftsBlurRadius: {component.sunShaftBlurRadius} to {Main.sunShaftsBlurRadius.Value}");
                Debug.Log($"SunShaftsBlurIterations: {component.radialBlurIterations} to {Main.sunShaftsRadialBlurIterations.Value}");
                Debug.Log($"SunShaftsIntensity: {component.sunShaftIntensity} to {Main.sunShaftsIntensity.Value}");
            }

            component.resolution = (SunShafts.SunShaftsResolution)Main.sunShaftsResolution.Value;
            component.screenBlendMode = (SunShafts.ShaftsScreenBlendMode)Main.sunShaftsScreenBlendMode.Value;
            component.maxRadius = Main.sunShaftsMaxRadius.Value;
            component.sunShaftBlurRadius = Main.sunShaftsBlurRadius.Value;
            component.radialBlurIterations = Main.sunShaftsRadialBlurIterations.Value;
            component.sunShaftIntensity = Main.sunShaftsIntensity.Value;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetBloom")]
        [HarmonyPrefix]
        private static void SetBloom()
        {
            if (postProcessingBehaviour.profile.bloom == null)
                return;

            BloomModel.Settings settings = postProcessingBehaviour.profile.bloom.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----Bloom settings----");
                Debug.Log($"BloomIntensity: {settings.bloom.intensity} to {Main.bloomIntensity.Value}");
                Debug.Log($"BloomAntiFlicker: {settings.bloom.antiFlicker} to {Main.bloomAntiFlicker.Value}");
                Debug.Log($"BloomRadious: {settings.bloom.radius} to {Main.bloomRadious.Value}");
                Debug.Log($"BloomSoftKnee: {settings.bloom.softKnee} to {Main.bloomSoftKnee.Value}");
                Debug.Log($"LensDirtIntensity: {settings.lensDirt.intensity} to {Main.lensDirtIntensity.Value}");
            }

            settings.bloom.intensity = Main.bloomIntensity.Value;
            settings.bloom.antiFlicker = Main.bloomAntiFlicker.Value;
            settings.bloom.radius = Main.bloomRadious.Value;
            settings.bloom.softKnee = Main.bloomSoftKnee.Value;
            settings.lensDirt.intensity = Main.lensDirtIntensity.Value;

            postProcessingBehaviour.profile.bloom.settings = settings;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetSSAO")]
        [HarmonyPostfix]
        private static void SetSSAO()
        {
            if (postProcessingBehaviour.profile.ambientOcclusion == null)
                return;

            AmbientOcclusionModel.Settings settings = postProcessingBehaviour.profile.ambientOcclusion.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----Ambient Oclusion settings----");
                Debug.Log($"SSAOintensity: {settings.intensity} to {Main.SSAOintensity.Value}");
                Debug.Log($"SSAOhighPrecision: {settings.highPrecision} to {Main.SSAOhighPrecision.Value}");
                Debug.Log($"SSAOambientOnly: {settings.ambientOnly} to {Main.SSAOambientOnly.Value}");
                Debug.Log($"SSAOdownSampling: {settings.downsampling} to {Main.SSAOdownSampling.Value}");
                Debug.Log($"SSAOfarDistance: {settings.farDistance} to {Main.SSAOfarDistance.Value}");
                Debug.Log($"SSAOsampleCount: {settings.sampleCount} to {(AmbientOcclusionModel.SampleCount)Main.SSAOsampleCount.Value}");
            }

            settings.intensity = Main.SSAOintensity.Value;
            settings.highPrecision = Main.SSAOhighPrecision.Value;
            settings.ambientOnly = Main.SSAOambientOnly.Value;
            settings.downsampling = Main.SSAOdownSampling.Value;
            settings.farDistance = Main.SSAOfarDistance.Value;
            settings.sampleCount = (AmbientOcclusionModel.SampleCount)Main.SSAOsampleCount.Value;

            postProcessingBehaviour.profile.ambientOcclusion.settings = settings;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetMotionBlur")]
        [HarmonyPostfix]
        private static void SetMotionBlur()
        {
            if (postProcessingBehaviour.profile.motionBlur == null)
                return;

            MotionBlurModel.Settings settings = postProcessingBehaviour.profile.motionBlur.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----MotionBlur settings----");
                Debug.Log($"FrameBlending: {settings.frameBlending} to {Main.MBframeBlending.Value}");
                Debug.Log($"SampleCount: {settings.sampleCount} to {Main.MBsampleCount.Value}");
                Debug.Log($"ShutterAngle: {settings.shutterAngle} to {Main.MBshutterAngle.Value}");
            }

            settings.frameBlending = Main.MBframeBlending.Value;
            settings.sampleCount = Main.MBsampleCount.Value;
            settings.shutterAngle = Main.MBshutterAngle.Value;

            postProcessingBehaviour.profile.motionBlur.settings = settings;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetAntiAliasing")]
        [HarmonyPostfix]
        private static void SetAntiAliasing()
        {
            if (postProcessingBehaviour.profile.antialiasing == null)
                return;

            AntialiasingModel.Settings settings = postProcessingBehaviour.profile.antialiasing.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----AntiAliasing settings----");
                Debug.Log($"Method: {settings.method} to {(AntialiasingModel.Method)Main.AAmethod.Value}");
            }

            settings.method = (AntialiasingModel.Method)Main.AAmethod.Value;

            postProcessingBehaviour.profile.antialiasing.settings = settings;
        }

        [HarmonyPatch(typeof(CameraEffects), "SetCA")]
        [HarmonyPostfix]
        private static void SetCA()
        {
            if (postProcessingBehaviour.profile.chromaticAberration == null)
                return;

            ChromaticAberrationModel.Settings settings = postProcessingBehaviour.profile.chromaticAberration.settings;

            if (Main.printValues.Value)
            {
                Debug.Log("----ChromaticAberration settings----");
                Debug.Log($"Intensity: {settings.intensity} to {Main.CAintensity.Value}");
            }

            settings.intensity = Main.CAintensity.Value;

            postProcessingBehaviour.profile.chromaticAberration.settings = settings;
        }
    }
}
