﻿using System;
using System.Collections.Generic;
using UnityEngine;
using HarmonyLib;

namespace GraphicsConfigPlus
{
    class HiddenSettings
    {
        public static void OnReload()
        {
            ApplyShadowQuality();
            ApplyQualitySettings();
            ApplyStartupSettings();
            Game_Start();
        }

        [HarmonyPatch(typeof(Settings), "ApplyShadowQuality")]
        [HarmonyPostfix]
        private static void ApplyShadowQuality()
        {
            if (Main.printValues.Value)
            {
                Debug.Log("----Shadow settings----");
                Debug.Log($"shadowResolution: {QualitySettings.shadowResolution} to {(ShadowResolution)Main.shadowResolution.Value}");
                Debug.Log($"shadowCascades: {QualitySettings.shadowCascades} to {Main.shadowCascades.Value}");
                Debug.Log($"shadowDistance: {QualitySettings.shadowDistance} to {Main.shadowDistance.Value}");
                Debug.Log($"shadows: {QualitySettings.shadows} to {(ShadowQuality)Main.shadows.Value}");
            }

            QualitySettings.shadowCascades = Main.shadowCascades.Value;
            QualitySettings.shadowDistance = Main.shadowDistance.Value;
            QualitySettings.shadows = (ShadowQuality)Main.shadows.Value;
            QualitySettings.shadowResolution = (ShadowResolution)Main.shadowResolution.Value;
        }

        [HarmonyPatch(typeof(Settings), "ApplyQualitySettings")]
        [HarmonyPostfix]
        private static void ApplyQualitySettings()
        {
            if (Main.printValues.Value)
            {
                Debug.Log("----General settings----");
                Debug.Log($"AnisotropicFiltering: {QualitySettings.anisotropicFiltering} to {(AnisotropicFiltering)Main.anisotropicFiltering.Value}");
                Debug.Log($"MasterTextureLimit: {QualitySettings.masterTextureLimit} to {Main.masterTextureLimit.Value}");
                if (SystemInfo.graphicsDeviceType != UnityEngine.Rendering.GraphicsDeviceType.Vulkan)
                    Debug.Log($"MaxQueuedFrames: {QualitySettings.maxQueuedFrames} to {Main.maxQueuedFrames.Value}");
                else
                    Debug.Log("Vulkan detected, not applying MaxQueuedFrames setting");
                Debug.Log($"ParticleRaycastBudget: {QualitySettings.particleRaycastBudget} to {Main.particleRaycastBudget.Value}");
                Debug.Log($"PixelLightCount: {QualitySettings.pixelLightCount} to {Main.pixelLightCount.Value}");
                Debug.Log($"RealtimeReflectionProbes: {QualitySettings.realtimeReflectionProbes} to {Main.realtimeReflectionProbes.Value}");
                Debug.Log($"SkinWeights: {QualitySettings.skinWeights} to {(SkinWeights)Main.skinWeights.Value}");
                Debug.Log($"SoftParticles: {QualitySettings.softParticles} to {Main.softParticles.Value}");
                Debug.Log($"SoftVegetation: {QualitySettings.softVegetation} to {Main.softVegetation.Value}");
                Debug.Log($"LodBias: {QualitySettings.lodBias} to {Main.lodBias.Value}");
            }

            QualitySettings.anisotropicFiltering = (AnisotropicFiltering)Main.anisotropicFiltering.Value;
            QualitySettings.lodBias = Main.lodBias.Value;
            QualitySettings.masterTextureLimit = Main.masterTextureLimit.Value;
            if(SystemInfo.graphicsDeviceType != UnityEngine.Rendering.GraphicsDeviceType.Vulkan)
                QualitySettings.maxQueuedFrames = Main.maxQueuedFrames.Value;
            QualitySettings.particleRaycastBudget = Main.particleRaycastBudget.Value;
            QualitySettings.pixelLightCount = Main.pixelLightCount.Value;
            QualitySettings.realtimeReflectionProbes = Main.realtimeReflectionProbes.Value;
            QualitySettings.skinWeights = (SkinWeights)Main.skinWeights.Value;
            QualitySettings.softParticles = Main.softParticles.Value;
            QualitySettings.softVegetation = Main.softVegetation.Value;
        }

        [HarmonyPatch(typeof(Game), "Start")]
        [HarmonyPostfix]
        private static void Game_Start()
        {
            Debug.Log($"MaxFPS: {Application.targetFrameRate} to {Main.maxFps.Value}");
            Application.targetFrameRate = Main.maxFps.Value;
        }

        [HarmonyPatch(typeof(Settings), "ApplyStartupSettings")]
        [HarmonyPostfix]
        private static void ApplyStartupSettings()
        {
            Debug.Log($"vSyncCount: {QualitySettings.vSyncCount} to {Main.vSyncCount.Value}");
            QualitySettings.vSyncCount = Main.vSyncCount.Value;
        }
    }
}
